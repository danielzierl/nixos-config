
{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./home.nix
    ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  hardware.bluetooth.enable = true;
  services.blueman.enable = true;

  programs.zsh.enable=true;
  users.defaultUserShell= pkgs.zsh;

   networking.hostName = "nixos"; # Define your hostname.
   networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

   time.timeZone = "Europe/Amsterdam";

   i18n.defaultLocale = "en_US.UTF-8";
   sound.enable = true;
   hardware.pulseaudio.enable = true;

   services.xserver.libinput.enable = true;
   hardware.opengl.enable=true;

   users.users.danikpapas= {
     isNormalUser = true;
     extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
     packages = with pkgs; [
       firefox
       tree
     ];
   };
  
   environment.shells = with pkgs; [zsh];
   fonts.fonts = with pkgs; [nerdfonts];
   fonts.fontDir.enable = true;
   environment.systemPackages =  with pkgs; [
	vim 
	git
	wget
  dunst
  blueman
  libnotify
  neovim
  python3
  unzip
	neofetch
	elogind
	seatd
  xfce.thunar
  brightnessctl
  llvmPackages_rocm.clang-tools-extra
	sway
	swaybg
  colloid-gtk-theme
  cpupower-gui
	chromium
	librewolf
	home-manager
	gcc
	glib
	alacritty
	kitty
  pavucontrol
	wofi
	waybar
	virt-manager
	gnome.gnome-tweaks
	gruvbox-dark-gtk
	gruvbox-gtk-theme
  htop

   ];
environment.sessionVariables = rec {
WLR_NO_HARDWARE_CURSORS = "1";
GTK_THEME="gruvbox-dark:dark";

};

   networking.firewall.enable = false;

   system.copySystemConfiguration = true;

  system.stateVersion = "23.05";

}

