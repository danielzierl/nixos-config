#! /bin/sh

cp -r ./config ~/.config
cp -r ./packer ~/.local/share/nvim/site/pack/packer/start/packer.nvim
cp -r ./scripts ~/.scripts
cp -r ./pictures ~/.pictures
