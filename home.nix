
let
	home-manager = builtins.fetchTarball {
		url = "https://github.com/nix-community/home-manager/archive/release-23.05.tar.gz";
	};
in
{
	imports = [(import "${home-manager}/nixos")];
	home-manager.users.danikpapas = {
		programs.git = {
			enable = true;
			userName = "Daniel Zierl";
			userEmail = "danielzierl@outlook.com";
		};
		home.stateVersion = "23.05";
		home.homeDirectory = "/home/danikpapas";
		home.username = "danikpapas";
		home.file.".config" = {
		source = ./config;
		recursive = true;
		};
		home.file.".local/share/nvim/site/pack/packer/start/packer.nvim" = {
			source = ./packer;
			recursive = true;
		};
		home.file.".pictures" = {
			source = ./pictures;
		};
		home.file.".scripts" = {
			source = ./scripts;
		};

	};
}
