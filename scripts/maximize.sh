#!/bin/sh

# Get the current layout tree
layout_tree=$(swaymsg -t get_tree)

# Check if the layout is tabbed
if echo "$layout_tree" | grep -q '"type": "con", "layout": "tabbed"'; then
    echo "Tabbed mode"
else
    echo "Not in tabbed mode"
fi
