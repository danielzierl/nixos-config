
#! /bin/sh
reg_match=$(pactl get-sink-volume 0 | grep -E -m 1 -o "[[:digit:]]{1,3}%" | head -1)
reg_match=$(echo $reg_match | grep -E -m 1 -o "[[:digit:]]*")
echo $reg_match
if [ "$1" = "up" ]
then
  if [ $reg_match -ge 75 ]
  then
    pactl set-sink-volume 0 100%;
  else
    pactl set-sink-volume 0 +15%;
  fi
elif [ "$1" = "mute" ]
then
    pactl set-sink-volume 0 0%;
else
  pactl set-sink-volume 0 -15%;
fi
reg_match=$(pactl get-sink-volume 0 | grep -E -m 1 -o "[[:digit:]]{1,3}%" | head -1)
dunstctl close-all
notify-send -t 800 "$(echo "Volume = $reg_match")";
