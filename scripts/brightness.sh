#! /bin/sh
if [ "$1" = "up" ]
then
  brightnessctl s +7%;
else
  brightnessctl s 7%-;
fi
curr=$(brightnessctl g);
echo $curr;
max=$(brightnessctl m);
echo $max;
result=$(awk -v a=$curr -v b=$max 'BEGIN {print (a/b*100)}' );
dunstctl close-all
notify-send -t 800 "$(echo "Brightness = $result%")";
